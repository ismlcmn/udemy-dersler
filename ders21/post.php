<?php session_start();

  $posts = $_POST;

  $username =  isset($posts["username"]) && !empty($posts["username"]) ? $posts["username"] : false;
  $password =  isset($posts["password"]) && !empty($posts["password"]) ? $posts["password"] : false;

  if($username && $password){

    $_SESSION["username"] = $username;
    $_SESSION["password"] = $password;
    $_SESSION["time"] = date("d-m-Y H:i:s");
    echo "Oturum Açıldı.";

    header("refresh:2;url=index.php");
    exit;

  }else{

    echo "Kullanıcı adı veya şifre boş";

  }

?>
